// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Inventory/InventoryElement.h"
#include "InventoryComponent.generated.h"

class UInventory;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInventoryItemAdded, const FInventoryElement&, InventoryElement);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HYDROSCHEMES_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	UPROPERTY(EditDefaultsOnly, Category = "UIWidget")
	TSubclassOf<UInventory> InventoryWidgetClass;
	UPROPERTY(VisibleAnywhere, Category = "UIWidget")
	TObjectPtr<UInventory> InventoryWidget;

	UPROPERTY(EditDefaultsOnly, Category="Inventory")
	TArray<FInventoryElement> Inventory;

	void AddItemToInventory(FInventoryElement& SchemeActor);
	void CreateInventoryWidget();
	bool IsInventoryWidgetValid();
	void OpenInventoryWidget();
	void CloseInventoryWidget();

	FOnInventoryItemAdded OnInventoryItemAdded;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
		
};
