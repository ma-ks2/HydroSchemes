// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunctionalComponents/SchemeState.h"
#include "EmptyInteractState.generated.h"

/**
 * 
 */
UCLASS()
class HYDROSCHEMES_API UEmptyInteractState : public USchemeState
{
	GENERATED_BODY()
};
