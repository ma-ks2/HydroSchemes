// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/PickUpInterface.h"
#include "UI/ShowText_Interface.h"
#include "Interfaces/InteractableInterface.h"
#include "SchemeActor.generated.h"

#define BOARD_CHANNEL ECC_GameTraceChannel2

class AHUBaseCharacter;
class UMaterialInstanceDynamic;
class UHintComponent;
class USchemeOutputHintComponent;
class USchemeState;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPickupInteractStateSet);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBoardInteractStateSet);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBeginViewOverlap); 
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndViewOverlap);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPickup);
//Abstract
UCLASS(Blueprintable)
class HYDROSCHEMES_API ASchemeActor : public AActor, public IPickUpInterface, public IInteractableInterface, public IShowText_Interface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASchemeActor();

	UPROPERTY(EditDefaultsOnly, Category="InventoryData")
	FName ActorName;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<USceneComponent> SceneComponent;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<UHintComponent> HintComponent;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<USchemeOutputHintComponent> SchemeOutputHintComponent;

	TWeakObjectPtr<UMaterialInstanceDynamic> HoloMaterialInstance;
	UPROPERTY(EditAnywhere, meta=(MetaClass="IInteractState"))
	TSubclassOf<USchemeState> StateClass;

	virtual void PickUpItem(FInventoryElement* InventoryElement) override;
	
	virtual void Interact() override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY()
	TObjectPtr<USchemeState> State;

public:	

	UPROPERTY(EditAnywhere, Category = "Interactable")
	float HintImageIndex;

	UPROPERTY(EditAnywhere, Category = "Interactable")
	float SchemeOutputHintImageIndex;

	UPROPERTY(EditDefaultsOnly)
	TEnumAsByte<ECollisionChannel> CollisionChannelToPlace;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AActor> ActorsToPlace;

	FOnPickupInteractStateSet OnPickupInteractStateSet;
	FOnBoardInteractStateSet OnBoardInteractStateSet;
	FOnBeginViewOverlap OnBeginViewOverlap;
	FOnEndViewOverlap OnEndViewOverlap;
	FOnPickup OnPickup;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void SetHoloMaterialInstance(TWeakObjectPtr<UMaterialInstanceDynamic> MaterialInstance);
	void SetHoloMaterialColor(FColor Color);
	virtual void SetResponseToChannel(ECollisionChannel CollisionChannel, ECollisionResponse CollisionResponse);
	virtual void SetRCT();
	virtual void AttachToCharacter(AHUBaseCharacter* Character, const FAttachmentTransformRules& AttachmentRules, FName SocketName);
	virtual void ShowHolo(AHUBaseCharacter* Character, const FHitResult HitResult);
	virtual ASchemeActor* CreateHolo(ASchemeActor* PlacingElement, UMaterialInterface* HoloMaterial);
	virtual void PlaceElement() {};
	virtual void PostActorCreated() override;
	virtual void Clone() {};
	void DisableCollision();
	void SetPickupInteractState();
	void SetBoardInteractState();

	UFUNCTION()
	virtual void ShowHint();
	UFUNCTION()
	virtual void HideHint();

	UFUNCTION()
	void ShowSchemeOutputHint();
	UFUNCTION()
	void HideSchemeOutputHint();
	/*
	* ������� ������� ������� ����� ����������� �� ������� 	FOnBeginViewOverlap OnBeginViewOverlap;	FOnEndViewOverlap OnEndViewOverlap;
	* ����� ����� ��������� ���������, ������ ����� ������� ���������. ������ ������� UE_LOG "��������� ��� ��������" GetName();
	* UE_LOG(LogTemp,Warning, TEXT("��������� ��� �������� %s"), *GetName())
	* ��� ������� ������ ����� UFUNCTION()
	* � BeginPlay � SchemeActor �� ���� ���������� ��� ���: OnScoreChangedDelegate.AddUniqueDynamic(this, &ThisClass::OnScoreChanged);
	*/
};
