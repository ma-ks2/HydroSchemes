// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalComponents/State.h"
#include "SchemeState.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnInteract);

/**
 * 
 */
UCLASS(Abstract)
class HYDROSCHEMES_API USchemeState : public UState
{
	GENERATED_BODY()

public:
	virtual inline void Interact(UObject* StateOwner = nullptr) {};
	FOnInteract OnInteract;
};
