// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalComponents/State.h"
#include "HoloState.generated.h"


/**
 *
 */
UCLASS(Abstract)
class HYDROSCHEMES_API UHoloState : public UState
{
	GENERATED_BODY()

public:
	virtual void PlaceElement() {};
	virtual void Interact() {};
	virtual void DeleteHolo() {};
	virtual void ShowHint() {};
};
