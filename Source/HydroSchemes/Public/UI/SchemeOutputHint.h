// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SchemeOutputHint.generated.h"

/**
 * 
 */

class UImage;//переменная изображения виджета

UCLASS()
class HYDROSCHEMES_API USchemeOutputHint : public UUserWidget
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UImage* HintImage;

	UPROPERTY(EditDefaultsOnly)
	FName ImageMaterialParameter;

	void SetImageIndex(float ImageIndex);
};
