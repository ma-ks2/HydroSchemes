// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InputHint.generated.h"

class UImage;
class UTextBlock;

/**
 * 
 */

USTRUCT(BlueprintType)
struct FInputHintModel
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	FString ActionText;
	UPROPERTY(EditAnywhere)
	int32 Id;
};

UCLASS()
class HYDROSCHEMES_API UInputHint : public UUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* Icon;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* ActionText;
	UPROPERTY(BlueprintReadOnly,VisibleAnywhere)
	int32 Id;
};
