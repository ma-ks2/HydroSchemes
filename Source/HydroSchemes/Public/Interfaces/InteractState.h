// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractState.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInteract);

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractState : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class HYDROSCHEMES_API IInteractState
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void Interact(UObject* StateOwner = nullptr) = 0;
	//FOnInteract OnInteract;
};
