// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CharacterState.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCharacterState : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class HYDROSCHEMES_API ICharacterState
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void Interact() = 0;
	virtual void DeleteHolo() = 0;
};
