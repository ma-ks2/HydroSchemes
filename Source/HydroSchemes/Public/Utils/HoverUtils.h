// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "HSActors\BoardSchemeActor.h"
#include "FunctionalComponents\HintComponent.h"
/**
 * 
 */

class HYDROSCHEMES_API HoverUtils
{
public:
	template <class T>
	static void Hover(T* &OldActor, UWorld* World);
	static void Hover(ABoardSchemeActor* OldActor, FName& OldSocket, FHitResult HitResult);
};

template<class T>
inline void HoverUtils::Hover(T* &OldActor, UWorld* World)
{
	//UE_LOG(LogTemp, Warning, TEXT("Old Hover"));
	APlayerController* Controller = UGameplayStatics::GetPlayerController(World, 0);
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(World, 0));
	FVector ViewLocation;
	FRotator ViewRotation;

	if (!Character) return;
	if (!Controller) return;
	Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);

	FVector Start = ViewLocation;
	FVector End = Start + ViewRotation.Vector() * 500.0f;

	FHitResult HitResult;
	bool IsHit = World->LineTraceSingleByChannel(HitResult, Start, End, INTERACT);
	Character->OnHoverHit.Broadcast(HitResult);

	T* HitActor = Cast<T>(HitResult.GetActor());
	if (IsHit && HitActor)
	{

		if (!HitResult.bBlockingHit) return;
		if (OldActor)
		{
			if (HitActor == OldActor) return;
			OldActor->OnEndViewOverlap.Broadcast();
			HitActor->OnBeginViewOverlap.Broadcast();
			OldActor = HitActor;
		}
		else
		{
			HitActor->OnBeginViewOverlap.Broadcast();
			OldActor = HitActor;
		}
	}
	else
	{
		if (OldActor != nullptr)
		{
			OldActor->OnEndViewOverlap.Broadcast();
			OldActor = nullptr;
		}
	}
}

inline void HoverUtils::Hover(ABoardSchemeActor* OldActor, FName& OldSocket, FHitResult HitResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("New Hover"));
	ABoardSchemeActor* BoardSchemeActor = Cast<ABoardSchemeActor>(HitResult.GetActor());
	if (!BoardSchemeActor) return;
	FName CurrentSocket = BoardSchemeActor->GetNearestSocketName(HitResult.ImpactPoint, ABoardSchemeActor::SocketInteractDistance);

	if (!CurrentSocket.IsNone())
	{

		if (!HitResult.bBlockingHit) return;
		if (!OldSocket.IsNone())
		{
			if (CurrentSocket == OldSocket) return;
			//UE_LOG(LogTemp, Warning, TEXT("if (CurrentSocket == OldSocket)"));
			OldActor->GetOnEndSocketViewOverlap().Broadcast();	
			OldSocket = CurrentSocket;
			BoardSchemeActor->GetOnBeginSocketViewOverlap().Broadcast();

		}
		else
		{
			OldSocket = CurrentSocket;
			BoardSchemeActor->GetOnBeginSocketViewOverlap().Broadcast();
		}
	}
	else
	{
		if (!OldSocket.IsNone())
		{
			UE_LOG(LogTemp, Warning, TEXT("if (!OldSocket.IsNone())"));
			OldActor->GetOnEndSocketViewOverlap().Broadcast();
			OldSocket = FName();
		}
	}
}
