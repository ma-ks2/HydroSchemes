// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunctionalComponents/HoloState.h"
#include "IncorrectHoloState.generated.h"

/**
 * 
 */
UCLASS()
class HYDROSCHEMES_API UIncorrectHoloState : public UHoloState
{
	GENERATED_BODY()
public: 
	virtual void DeleteHolo() override;
};
