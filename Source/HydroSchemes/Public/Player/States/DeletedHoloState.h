// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunctionalComponents/HoloState.h"
#include "DeletedHoloState.generated.h"

/**
 * 
 */
UCLASS()
class HYDROSCHEMES_API UDeletedHoloState : public UHoloState
{
	GENERATED_BODY()
public:
	virtual void Interact() override;
	virtual void ShowHint() override;
};
