// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunctionalComponents/InputModeState.h"
#include "GameAndUIState.generated.h"

/**
 * 
 */
UCLASS()
class HYDROSCHEMES_API UGameAndUIState : public UInputModeState
{
	GENERATED_BODY()
public:
	virtual void Interact() override;
	virtual void DeleteHolo() override;
	virtual void ShowHint() override;
};
