// HydroSchemes Simulator. All rights reserved.


#include "FunctionalComponents/SchemeOutputHintComponent.h"
#include "Kismet/GameplayStatics.h"
#include "UI/SchemeOutputHint.h"
#include "HSActors/SchemeActor.h"

// Sets default values for this component's properties
USchemeOutputHintComponent::USchemeOutputHintComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


//// Called when the game starts
//void USchemeOutputHintComponent::BeginPlay()
//{
//	Super::BeginPlay();
//
//	// ...
//	
//}


void USchemeOutputHintComponent::Init()
{
	CreateSchemeOutputHintWidget();
	ASchemeActor* SchemeActor = Cast<ASchemeActor>(GetOwner());
	SchemeOutputHintWidget->SetImageIndex(SchemeActor->SchemeOutputHintImageIndex);
}

// Called every frame
void USchemeOutputHintComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USchemeOutputHintComponent::CreateSchemeOutputHintWidget()
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (!PlayerController) return;
	SchemeOutputHintWidget = CreateWidget<USchemeOutputHint>(PlayerController, SchemeOutputHintWidgetClass);
	SchemeOutputHintWidget->AddToViewport(1);
	SchemeOutputHintWidget->SetVisibility(ESlateVisibility::Hidden);
}

void USchemeOutputHintComponent::OpenSchemeOutputHintWidget()
{
	SchemeOutputHintWidget->SetVisibility(ESlateVisibility::Visible);
}

void USchemeOutputHintComponent::CloseSchemeOutputHintWidget()
{
	SchemeOutputHintWidget->SetVisibility(ESlateVisibility::Hidden);
}

