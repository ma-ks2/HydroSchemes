// HydroSchemes Simulator. All rights reserved.


#include "UI/HintCable.h"
#include "Components/TextBlock.h"
#include "Components/Border.h"
#include "Components/Image.h"

void UHintCable::SetHintHeaderTextLink(FText InputText)
{
	HintTextLink->SetText(InputText);
}

void UHintCable::SetHintSocketTextLink(FText InputText)
{
	HintInputSocketLink->SetText(InputText);
}

void UHintCable::SetImageIndexLink(float ImageIndex)
{
	HintImageLink->GetDynamicMaterial()->SetScalarParameterValue(ImageMaterialParameterLink, ImageIndex);
}
