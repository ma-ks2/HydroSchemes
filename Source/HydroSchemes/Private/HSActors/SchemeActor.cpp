// HydroSchemes Simulator. All rights reserved.


#include "HSActors/SchemeActor.h"
#include "Inventory/InventoryComponent.h"
#include "Engine/CollisionProfile.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Player/HUBaseCharacter.h"
#include "Interfaces/PickUpInterface.h"
#include "HSActors/States/BoardInteractState.h"
#include "HSActors/States/PickupInteractState.h"
#include "FunctionalComponents/HintComponent.h"
#include "FunctionalComponents/SchemeOutputHintComponent.h"
#include "Kismet/GameplayStatics.h"
#include "FunctionalComponents/SchemeState.h"
#include "FunctionalComponents\HintComponent.h"

#define ECC_Interact ECC_GameTraceChannel1

DEFINE_LOG_CATEGORY_STATIC(LogSchemeActor, All, All)

// Sets default values
ASchemeActor::ASchemeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	RootComponent = SceneComponent;
	HintComponent = CreateDefaultSubobject<UHintComponent>("HintComponent");
	SchemeOutputHintComponent = CreateDefaultSubobject<USchemeOutputHintComponent>("SchemeOutputHintComponent");
}

void ASchemeActor::PickUpItem(FInventoryElement* InventoryElement)
{
	InventoryElement->ElementClass = GetClass();
	InventoryElement->ElementName = ActorName;
	OnPickup.Broadcast();
	Destroy();
}

void ASchemeActor::Interact()
{
	State->Interact(this);
}

// Called when the game starts or when spawned
void ASchemeActor::BeginPlay()
{
	Super::BeginPlay();
	SetPickupInteractState();
	HintComponent->Init();
	SchemeOutputHintComponent->Init();

	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;

	OnBeginViewOverlap.AddDynamic(this, &ThisClass::ShowHint);
	OnEndViewOverlap.AddDynamic(this, &ThisClass::HideHint);
	OnPickup.AddDynamic(this, &ASchemeActor::HideHint);

	OnBeginViewOverlap.AddDynamic(this, &ThisClass::ShowSchemeOutputHint);
	OnEndViewOverlap.AddDynamic(this, &ThisClass::HideSchemeOutputHint);
	OnPickup.AddDynamic(this, &ASchemeActor::HideSchemeOutputHint);
}

// Called every frame
void ASchemeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASchemeActor::SetHoloMaterialInstance(TWeakObjectPtr<UMaterialInstanceDynamic> MaterialInstance)
{
	HoloMaterialInstance = MaterialInstance;
	
	TArray<UStaticMeshComponent*> StaticMeshComponents;
	GetComponents<UStaticMeshComponent>(StaticMeshComponents);

	for (UStaticMeshComponent* SMC : StaticMeshComponents) {
		if (SMC) {
			for (int32 i = 0; i < SMC->GetNumMaterials(); i++) {
				SMC->SetMaterial(i, HoloMaterialInstance.Get());
			}
		}
	}

}

void ASchemeActor::SetHoloMaterialColor(FColor Color)
{
	HoloMaterialInstance->SetVectorParameterValue(FName("Color"), Color);
}

void ASchemeActor::SetResponseToChannel(ECollisionChannel CollisionChannel, ECollisionResponse CollisionResponse){}

void ASchemeActor::SetRCT(){}

void ASchemeActor::AttachToCharacter(AHUBaseCharacter* Character, const FAttachmentTransformRules& AttachmentRules, FName SocketName)
{
	AttachToComponent(Character->GetMesh(), AttachmentRules, SocketName);
}

void ASchemeActor::ShowHolo(AHUBaseCharacter* Character, const FHitResult HitResult)
{
}

ASchemeActor* ASchemeActor::CreateHolo(ASchemeActor* PlacingElement, UMaterialInterface* HoloMaterial)
{
	return nullptr;
}

void ASchemeActor::PostActorCreated()
{
	Super::PostActorCreated();
	SetPickupInteractState();
}

void ASchemeActor::DisableCollision()
{
	TArray<UStaticMeshComponent*> StaticMeshComponents;
	GetComponents<UStaticMeshComponent>(StaticMeshComponents);

	for (UStaticMeshComponent* SMC : StaticMeshComponents) {
		if (SMC) {
			SMC->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}
	}
}

void ASchemeActor::SetPickupInteractState()
{
	UPickupInteractState* PickupInteractState = NewObject<UPickupInteractState>(this);
	State = PickupInteractState;
	OnPickupInteractStateSet.Broadcast();
}

void ASchemeActor::SetBoardInteractState()
{
	UBoardInteractState* BoardInteractState = NewObject<UBoardInteractState>(this);
	State = BoardInteractState;
	OnPickupInteractStateSet.Broadcast();
}

void ASchemeActor::ShowHint()
{
	HintComponent->OpenHintWidget();
}

void ASchemeActor::HideHint()
{
	HintComponent->CloseHintWidget();
}

void ASchemeActor::ShowSchemeOutputHint()
{
	//check(State.GetObject());
	/*if (State.GetObject()->IsA<UBoardInteractState>())
	{
		UBoardInteractState* BoardInteractState = Cast<UBoardInteractState>(State.GetObject());
		BoardInteractState->ShowSchemeOutputHint(this);
	}
	*/
}

void ASchemeActor::HideSchemeOutputHint()
{
	SchemeOutputHintComponent->CloseSchemeOutputHintWidget();
}