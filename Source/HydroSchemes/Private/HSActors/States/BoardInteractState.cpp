// HydroSchemes Simulator. All rights reserved.


#include "HSActors/States/BoardInteractState.h"
#include "HSActors/SchemeActor.h"
#include "FunctionalComponents/SchemeOutputHintComponent.h"

void UBoardInteractState::Interact(UObject* StateOwner)
{
	OnInteract.Broadcast();
}

void UBoardInteractState::ShowSchemeOutputHint(ASchemeActor* Owner)
{
	Owner->SchemeOutputHintComponent->OpenSchemeOutputHintWidget();
}
